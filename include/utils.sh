#!/usr/bin/env bash

# command exists
command_exists() {
    which "$@" > /dev/null 2>&1
	# command -v "$@" > /dev/null 2>&1
}

# info message
info_message() {
	echo -e "\n\n\e[30;47m■■■■■■ ${1} ■■■■■■      \e[0m"
}

# pass message
pass_message() {
	echo -e "\n\e[1;92m■■■ ${1} \e[0m"
}

# err message
err_message() {
	echo -e "\n\e[1;31m${1}\e[0m"
	exit 1
}

# wineapp url
WINEAPP_URL="https://jihulab.com/wineapp/wineapp.git"

# app center
WINEAPP_CENTER_URL="https://jihulab.com/wineapp/winecenter.git"

# WINEPREFIX
WINEPREFIX="${HOME}/.wine"

# WINE_DEVICES
WINE_DEVICES_PATH="${HOME}/.wine/dosdevices/"

# wine app tool
WINEAPP_PATH="${HOME}/.wineapp"

# app install prefix
APPPREFIX_PATH="${HOME}/.config/wineapp"

# wineapp run log
WINEAPP_LOG="${WINEAPP_PATH}/wineapp.log"
