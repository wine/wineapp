#!/usr/bin/env bash

load() {
    local BASH_REAL_PATH=$(dirname "`readlink -f $0`")
    RUN_REAL_PATH=${BASH_REAL_PATH%/*}
    . "${RUN_REAL_PATH}/include/utils.sh"
}

check_args() {
    [[ $# -lt 3 ]] && err_message "The parameters must be greater than 3"
}

main() {
    load

    check_args "$@"

    EXE_REAL_PATH="${3}"
    # echo "${EXE_REAL_PATH}"

    # EXE_FULL_PATH="${WINE_DEVICES_PATH}${EXE_REAL_PATH}"
    # ls "${EXE_FULL_PATH}"
    # echo "${EXE_FULL_PATH}"
    # wine "${EXE_FULL_PATH}"

    wine start /Unix "${EXE_REAL_PATH}" >${WINEAPP_LOG} 2>&1 
}

main "$@" || exit 1