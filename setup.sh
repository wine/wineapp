#!/usr/bin/env bash

## 加载类库与环境
load() {
    local BASH_REAL_PATH=$(dirname "`readlink -f $0`")
    . "${BASH_REAL_PATH}/include/utils.sh"
}

## 初始化
init() {
    if [ -f /etc/lsb-release ]; then
        source /etc/lsb-release
    fi

    if ! command_exists wget ; then
        sudo apt install -y wget
    fi

    if ! command_exists git ; then
        sudo apt install -y git
    fi
}

# 前置环境检测
pre_env_testing() {
    # 当前仅支持 Ubuntu 
    if [ "${DISTRIB_ID}" != "Ubuntu" ]; then
        err_message "OS ${DISTRIB_ID} is not supported by this installation script"
    fi

    # 检测操作系统架构
    ARCHITECTURE=$(dpkg --print-architecture && dpkg --print-foreign-architectures)
    if ! echo "${ARCHITECTURE}" | grep -qE 'amd64|i386'; then
        err_message "Only amd64 and i386 are supported"
    fi
    echo "${ARCHITECTURE}" | grep -qE 'i386' || dpkg --add-architecture i386
}

## 安装 WINE
install_wine() {
    # 下载添加仓库密钥
    if [ ! -f /usr/share/keyrings/winehq-archive.key ]; then
        wget -qO - https://dl.winehq.org/wine-builds/winehq.key \
            | gpg --dearmor \
            | sudo dd of=/usr/share/keyrings/winehq-archive.key
    fi

    # 添加仓库
    if [ ! -f /etc/apt/sources.list.d/winehq-${DISTRIB_CODENAME}.sources ]; then
        wget -qO - "https://dl.winehq.org/wine-builds/ubuntu/dists/${DISTRIB_CODENAME}/winehq-${DISTRIB_CODENAME}.sources" \
            | sudo tee /etc/apt/sources.list.d/winehq-${DISTRIB_CODENAME}.sources

        # 更新安装包
        sudo apt update -y
    fi

    # 安装 wine
    local WINEHQ_INSTALLED=$(dpkg --list | grep winehq-devel)
    if [ -z "${WINEHQ_INSTALLED}" ]; then
        ## 稳定分支 （经测，没有此安装包，不能正常安装）
        # apt install --install-recommends winehq-stable
        ## 开发分支	（本人使用此分支）
        sudo apt install --install-recommends winehq-devel -y
        ## Staging 分支	
        # apt install --install-recommends winehq-staging
    fi
}

## 安装本项目工具
install_tools() {
    local TMP_WINE_PROJECT="/tmp/wineapp"

    [[ -d "${TMP_WINE_PROJECT}" ]] && rm -rf "${TMP_WINE_PROJECT}"
    [[ -d "${WINEAPP_PATH}" ]] || mkdir ${WINEAPP_PATH}
    
    MODE=""
    if [[ "${MODE}" = "DEV" ]]; then
        # dev
        cp -r ../wineapp/* "${WINEAPP_PATH}"
 
    else
        # prod
        git clone "${WINEAPP_URL}" "${TMP_WINE_PROJECT}" >/dev/null 2>&1

        if [[ -d "${TMP_WINE_PROJECT}" ]]; then   
            cp -r ${TMP_WINE_PROJECT}/* ${WINEAPP_PATH}
        else
            err_message "WINEAPP_URL(${WINEAPP_URL}) invalid"
        fi
    fi

    set_config

    source "${HOME}/.bashrc" && wineapp update
}

## 设置 XDG_DATA_DIRS
set_config() {
    # update env
    sed -i "s@^WINEAPP_PATH.*@WINEAPP_PATH=\"${WINEAPP_PATH}\"@" "${WINEAPP_PATH}/env"
    
    [[ -d "${APPPREFIX_PATH}" ]] || mkdir "${APPPREFIX_PATH}"

    # 设置环境变量
	if [ -z "`grep ${WINEAPP_PATH}/env ${HOME}/.bashrc`" ];then
        echo -e "\n## wineapp" >> "${HOME}/.bashrc"
        echo ". \"${WINEAPP_PATH}/env\"" >> "${HOME}/.bashrc"
	fi 

    # 更新 XDG_DATA_DIRS 中的环境变量目录
    sed -i "s@/opt/apps@${APPPREFIX_PATH}@" "${WINEAPP_PATH}/env"
}

setup() {
    load

    init

    pre_env_testing

    install_wine

    install_tools
}

main() {
    local TMP_WINE_CENTER="/tmp/wineapp-center"

    [[ -d "${TMP_WINE_CENTER}" ]] && rm -rf "${TMP_WINE_CENTER}"
    [[ -d "${WINEAPP_PATH}/center" ]] && mv "${WINEAPP_PATH}/center" "${TMP_WINE_CENTER}"

    setup

    [[ -d "${TMP_WINE_CENTER}" ]] && mv "${TMP_WINE_CENTER}" "${WINEAPP_PATH}/center"
}

main "$@" || exit 1
