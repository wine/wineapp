## 使用 Wine 在 Linux 上运行 Windows 程序
> Using Wine to run Windows programs in Linux.

### 安装 Wine 教程
- https://www.cnblogs.com/jetsung/p/Using-Wine-to-run-Windows-programs-in-Linux.html
```bash
wget -O- https://jihulab.com/wineapp/wineapp/-/raw/main/setup.sh | bash
```